# Setup guide

Due to the size of the imported assets, it takes longer to manage them with git rather than manually putting them into the project folder.

From the google drive link I left in #resources, open the folder "zips" and download all the archives beginning with "HAS".
Inside the folder the repo was cloned into, open the "Assets" folder and create a sub-folder named "Imports".
Inside of Imports, extract each archive into a separate folder.
Once you're done, open Unity Hub (or the Unity editor) and open the folder of the repository as a Unity project.

You'll know if you did it right if it looks like this in the end:

![Folder structure](https://cdn.discordapp.com/attachments/905552747913429025/905552779068706836/unknown.png)

# Game Story and Infrastructure

## Basic Ideea
The player will get a prophetic dream where they will learn of an upcoming war between the land's factions. In the upcoming week they must decide the fate of their world by talking with the leaders of the different factions and getting them to join his side.

## Game Story
It all started during a stormy night… Terrifying thunders could be heard rumbling in the endless sky accompanied by angry lightnings, while the rain was vengefully pattering agains the fragile windows. It was during this same night that our so called hero, Eyvor by name, was visited in his dreams by a mythical creature who warned him of an upcoming calamity that only he was able to prevent from happening. The two most powerful kingdoms in the area, Ikeoryn and Namore Kingdoms, were going at war against each other in 7 days. In order to stop that from happening, our hero has to travel to both kingdoms and earn the trust of their leaders, while hoping to change their minds and avoid a massive bloodshed. With this plan in mind, in the middle of the night, our hero puts the saddle on his horse and starts riding to his first adventure… there is no time to waste, may the faith be in Eyvor’s favor.

## Features/Mechanics:

A rich story;
Replayability;
Minigames: Each interaction with an NPC consists of a possible challenge. You have to win a minigame in order for him to join you. 
- New Game +: After you finish the game once, you can play it multiple times in order to unlock more endings, secrets and bonuses;
- Save/Load game;

## Minigames:

There are 2 main kingdoms , Ikeorin and Namore, each with its own minigame type. Also the training grounds consist in a minigame which should be played before entering the kingdoms:
* The Tower/Magic kingdom: A rhythm based minigame; Practicing magic is all about rhythm and focus, you’ll have to prove you have what it takes to impress the king.
* The Stronghold kingdom: A battle minigame; Residents of the stronghold area are fierce fighters, battling and conflict is in their nature. To get them on your side, you’ll have to prove how strong you are by fighting.
* The Training facilities: A Flappy bird minigame; Humans are really good tamers and griffin riders. To win over the humans, you’ll have to prove you can ride a dragon successfully.

## Game Interface

We created a 2D fantasy game based on oldschool & pixelart inspiration. We will attatch screenshots with the menu & the game interfaces. 

* Menu

* Flappy Dragon (Training Grounds)

* Ikeoryn Kingdom (Battle Game)

* Namore KIngdom (Rythm Game)

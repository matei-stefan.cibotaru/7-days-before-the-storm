using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupController : MonoBehaviour
{
    public float maxCooldown = 1f, cooldown = 0f;
    TextMeshProUGUI text;

    void Awake() {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
    }

    public void ChangePopup(Color c, string s) {
        text.text = s;
        text.color = c;
        transform.rotation = Quaternion.Euler(0, 0, Random.value * 20 - 10);
        cooldown = maxCooldown;
    }

    // Update is called once per frame
    void Update() {
        if(cooldown < 0) {
            cooldown = 0;
            text.alpha = 0;
        }
        if(cooldown == 0) return;
        
        text.alpha = Mathf.Sin(cooldown / maxCooldown * Mathf.PI / 2);

        cooldown -= Time.deltaTime;
    }
}

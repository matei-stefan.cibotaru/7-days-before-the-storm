using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissNote : MonoBehaviour
{
    RhythmController rhythmController;

    void OnTriggerEnter2D(Collider2D collider) {
        if(collider.GetType() == typeof(BoxCollider2D)) {
            Destroy(collider.transform.parent.gameObject);
            rhythmController.UpdateScore(NoteController.accuracy.none);
        }
    }

    void Awake() {
        rhythmController = GameObject.Find("controls").GetComponent<RhythmController>();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongManager : MonoBehaviour
{
    Transform notesObj;
    Dictionary<NoteData.orientation, Transform> controls = new Dictionary<NoteData.orientation, Transform>();
    GameObject notePrefab;

    void Start() {
        notesObj = transform.Find("notes");
        var controlsObj = transform.Find("controls");
        for(int i = 0; i < 4; i++) {
            controls.Add((NoteData.orientation) i, controlsObj.GetChild(i).transform);
        }
    }

    void Update() {
        //checks what command to trigger
        if(Input.GetKeyDown(KeyCode.S)) {
            SaveSong();
        }
        if(Input.GetKeyDown(KeyCode.L)) {
            LoadSong();
        }
        if(Input.GetKeyDown(KeyCode.C)) {
            ClearNotes();
        }
    }

    void LoadSong() {
        print("loading");
        var notesList = JsonUtility.FromJson<List<NoteData>>(System.IO.File.ReadAllText(Application.dataPath + "/Songs/sample.json"));

        foreach (NoteData nd in notesList) {
            GameObject newNote = Instantiate(notePrefab, new Vector3(controls[nd.ori].position.x, nd.position, 0), Quaternion.identity);
            newNote.GetComponent<NoteController>().SetupNote((int) nd.ori);
        }
    }

    void SaveSong() {
        print("saving");
        var notesList = new List<NoteData>();

        foreach (Transform note in notesObj) {
            NoteData nd = new NoteData();
            nd.position = note.position.y;
            nd.ori = note.GetComponent<NoteController>().orientation;
            notesList.Add(nd);
        }

        System.IO.File.WriteAllText(Application.dataPath + "/Songs/sample.json", JsonUtility.ToJson(notesList));
        print($"Successfully saved song data to {Application.dataPath}/Songs/sample.json");
    }

    void ClearNotes() {
        print("clearing");
        var notes = new List<GameObject>();
        
        foreach (Transform note in notesObj) {
            notes.Add(note.gameObject);
        }

        notes.ForEach(note => Destroy(note));
    }
}
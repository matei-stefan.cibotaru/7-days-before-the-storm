using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using accuracy = NoteController.accuracy;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class RhythmController : MonoBehaviour
{
    public ArrowController[] arrows = new ArrowController[4];
    private Dictionary<string, Color> accColors = new Dictionary<string, Color>() {
        {"miss", Color.red},
        {"bad", Color.yellow},
        {"ok", Color.green},
        {"good", Color.cyan},
        {"perfect", Color.magenta}
    };
    public Sprite[] sprites = new Sprite[4];
    public bool doneLoading = false;
    private float[,] noteDurations = {
        {0.008f, 0.024f},
        {0.024f, 0.062f},
        {0.048f, 0.126f},
        {0.084f, 0.218f}
    };
    public float[,] noteSizes = {
        {0, 0},
        {0, 0},
        {0, 0},
        {0, 0}
    };
    public GameObject notes;
    public PopupController popupController;
    public TextMeshProUGUI scoreDisplay;
    private AudioSource audioSource;
    public float noteSpeed = 2f;
    public int score = 0;

    void Awake() {
        scoreDisplay.text = "0";
        audioSource = GetComponent<AudioSource>();

        for(int i = 0; i < 4; i++) {
            var arrow = transform.GetChild(i).GetComponent<ArrowController>();
            var tex = Resources.Load<Texture2D>($"{arrow.orientation.ToString()}Arrow");
            var sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            sprites[i] = sprite;
            arrow.spriteRenderer.sprite = sprites[i];
            arrows[i] = arrow;
        }

        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 2; j++) {
                noteSizes[i,j] = noteSpeed * noteDurations[i,j];
            }
        }

        for(int i = 0; i < notes.transform.childCount; i++) {
            var note = notes.transform.GetChild(i).GetComponent<NoteController>();
            note.SetupNote((int)note.orientation);
        }

        doneLoading = true;
        StartCoroutine(songPlayer());
        //audioSource.Play();
    }

    IEnumerator songPlayer() {
        yield return new WaitForSeconds(2);
        audioSource.Play();
    }

    public void UpdateScore(accuracy acc) {
        switch(acc) {
            case accuracy.perfect:
                score += 10;
                popupController.ChangePopup(Color.cyan, "Perfect!");
                break;
            case accuracy.good:
                score += 6;
                popupController.ChangePopup(Color.green, "Good!");
                break;
            case accuracy.ok:
                score += 2;
                popupController.ChangePopup(Color.yellow, "OK!");
                break;
            case accuracy.bad:
                score += -4;
                popupController.ChangePopup(Color.red, "Bad!");
                break;
            default:
                score += -10;
                popupController.ChangePopup(Color.black, "Miss!");
                break;
        }
        scoreDisplay.text = score.ToString();
    }
}
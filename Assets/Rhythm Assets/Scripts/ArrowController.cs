using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using accuracy = NoteController.accuracy;

public class ArrowController : MonoBehaviour
{
    private SortedSet<BoxCollider2D> collisions = new SortedSet<BoxCollider2D>(new ColliderComparer());
    public SpriteRenderer spriteRenderer;
    public KeyCode keyCode;
    public NoteData.orientation orientation;
    public RhythmController rc;

    void OnTriggerEnter2D(Collider2D collider) {
        if(collider.GetType() == typeof(BoxCollider2D)) {
            collisions.Add((BoxCollider2D)collider);
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if(collider.GetType() == typeof(BoxCollider2D)) {
            collisions.Remove((BoxCollider2D)collider);
        }
    }

    public accuracy RemoveNote() {
        if(collisions.Count == 0) return accuracy.none;
        
        accuracy acc = collisions.Min.gameObject.GetComponent<NoteCollider>().acc;
        Transform note = collisions.Min.transform.parent;
        
        foreach (var col in note.GetComponentsInChildren<BoxCollider2D>()) {
            collisions.Remove(col);
        }
        Destroy(note.gameObject);

        return acc;
    }

    void Update() {
        if(Input.GetKeyDown(keyCode)) {
            var acc = RemoveNote();
            if(acc != accuracy.none) rc.UpdateScore(acc);
        }
    }
}

internal class ColliderComparer : IComparer<BoxCollider2D>
{
    public int Compare(BoxCollider2D x, BoxCollider2D y) {
        return x.size.y.CompareTo(y.size.y);
    }
}
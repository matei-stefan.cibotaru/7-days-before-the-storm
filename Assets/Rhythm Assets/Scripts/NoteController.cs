using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NoteController : MonoBehaviour
{
    private float speed = 1.18f;
    public enum accuracy {
        perfect, good, ok, bad, none
    }
    public NoteData.orientation orientation;
    private RhythmController rc;
    private SpriteRenderer spriteRenderer;
    private static bool editMode = false;

    public void SetupNote(int orientation) {
        //setting up the orientation, aligning the note and setting the correct sprite
        this.orientation = (NoteData.orientation)orientation;
        transform.position = new Vector3(rc.arrows[orientation].transform.position.x, transform.position.y, 0);
        spriteRenderer.sprite = rc.sprites[orientation];
        
        //setting up the colliders
        for(int i = 0; i < 4; i++) {
            var col = transform.GetChild(i).GetComponent<BoxCollider2D>();
            col.size = new Vector2(1, rc.noteSizes[i, 0] + rc.noteSizes[i, 1]);
            col.offset = new Vector2(0, (rc.noteSizes[i, 1] - rc.noteSizes[i, 0]) / 2);
        }
    }

    void Awake() {
        rc = GameObject.Find("controls").GetComponent<RhythmController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update() {
        if(!rc.doneLoading || editMode) return;
        transform.position += Vector3.up * speed * Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("ChooseFaction");
        }
    }
}

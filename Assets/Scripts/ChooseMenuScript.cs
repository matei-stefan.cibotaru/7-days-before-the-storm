using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseMenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    public void PlayGameIkeoryn()
    {
        SceneManager.LoadScene("Walkaround");
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayGameNamore()
    {
        SceneManager.LoadScene("Rhythm");
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayGameTraining()
    {
        SceneManager.LoadScene("FlappyDragon");
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

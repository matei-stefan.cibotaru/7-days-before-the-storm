using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{

    public float moveSpeed = 1f;
    // Start is called before the first frame update


    // Update is called once per frame
    void FixedUpdate()
    {
        //miscam scena inspre pietre
        transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x - moveSpeed, transform.position.y), 0.1f);


    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{

    public GameObject rockPrefab;
    public int generationRate = 100;

    // private int timer = 100;
    private int timer;
 

    // Update is called once per frame
    void FixedUpdate()
    {
        timer++;
        if(timer >= generationRate)
        {
            timer = 0;
            GameObject newObstacle = Instantiate(rockPrefab, new Vector2(rockPrefab.transform.position.x, rockPrefab.transform.position.y + Random.Range(-2f, 2f)), rockPrefab.transform.rotation);
            Destroy(newObstacle, 5f);
        }
    }
}
